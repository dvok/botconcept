﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class StateView : MonoBehaviour {

	private RectTransform rectTransform;
	private BoxCollider2D coll;
	private Image image;
	private Text text;

	private bool IsDragged;

	public Vector2 Position {
		get { 
			
			Vector3 pos = GetComponent<RectTransform> ().position;
			Debug.Log (this.Name+": "+pos);
			return new Vector2 (pos.x, pos.y);
		}
	}

	public string Name = "State";

	private void RetrieveComponents() {
		image = GetComponent<Image> ();
		rectTransform = GetComponent<RectTransform> ();
		text = GetComponentInChildren<Text> ();
		coll = GetComponent<BoxCollider2D> ();
	}

	// Use this for initialization
	void Start () {



		RetrieveComponents ();

		text.text = Name;
		IsDragged = false;
		coll.size = new Vector2 (rectTransform.rect.width, rectTransform.rect.height);
//		image.color = Color.cyan;
//		rectTransform.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, 50f);
	}

	// Update is called once per frame
	void Update () {
	
		if (IsDragged) {
			transform.position = Vector3.Lerp (transform.position, Input.mousePosition, 0.5f);
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
//		if (other.gameObject.layer == LayerMask.NameToLayer ("UI")) {
			Debug.Log (this.name + " collided with " + other.gameObject.name);
//		}
	}


	public void OnBeginDrag() {
		IsDragged = true;
	}

	public void OnEndDrag() {
		IsDragged = false;
	}

}
