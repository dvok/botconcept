﻿using UnityEngine;
using System.Collections;

public class State: MonoBehaviour {

	public string Name;

	public State() {
	}

	public State(string name) {
		Name = name;
	}
}
