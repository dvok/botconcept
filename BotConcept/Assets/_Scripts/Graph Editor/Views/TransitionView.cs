﻿using UnityEngine;
using System.Collections;

public class TransitionView : MonoBehaviour {

	public StateView StateFrom;
	public StateView StateTo;

	void OnGUI () {
		GuiHelper.DrawLine (StateFrom.Position, StateTo.Position, Color.black, 2);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
