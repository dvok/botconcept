﻿using UnityEngine;
using System.Collections;

public class Transition: MonoBehaviour {
	
	public State StateFrom;
	public State StateTo;

	public Transition() {
	}

	public Transition (State stateFrom, State stateTo) {
		StateFrom = stateFrom;
		StateTo = stateTo;
	}
}
